from django.conf.urls import patterns, include, url
from django.contrib import admin

from store.views import (EntertainmentView, AddToBasketView, BasketView, DelFromBasketView, ConfirmView, PDFView,
    PaymentSuccessView, PaymentFailureView, PayUrlView, OrderItemsView)


urlpatterns = patterns(
    '',
    url(r'^$',                      EntertainmentView.as_view(),    name='home'),
    url(r'^add_to_basket$',         AddToBasketView.as_view(),      name='add_to_basket'),
    url(r'^del_from_basket$',       DelFromBasketView.as_view(),    name='del_from_basket'),
    url(r'^basket$',                BasketView.as_view(),           name='basket'),
    url(r'^pdf/(?P<code>[-_\w]+)$', PDFView.as_view(),              name='pdf'),
    url(r'^confirm$',               ConfirmView.as_view(),          name='confirm'),
    url(r'^payment_success$',       PaymentSuccessView.as_view(),   name='payment_success'),
    url(r'^payment_failure$',       PaymentFailureView.as_view(),   name='payment_failure'),
    url(r'^pay_url$',               PayUrlView.as_view(),           name='pay_url'),
    url(r'^order_items',            OrderItemsView.as_view(),       name='order_items'),
    url(r'^admin/', include(admin.site.urls)),
)
