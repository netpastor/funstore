# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0013_auto_20150406_1232'),
    ]

    operations = [
        migrations.CreateModel(
            name='Entertainment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True)),
                ('price', models.PositiveIntegerField(default=0, verbose_name='\u0426\u0435\u043d\u0430')),
                ('revenue', models.PositiveIntegerField(default=0, verbose_name='\u0421\u0435\u0431\u0435\u0441\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c')),
                ('conditions', models.CharField(max_length=255, verbose_name='\u0423\u0441\u043b\u043e\u0432\u0438\u044f', blank=True)),
                ('explanations', models.CharField(max_length=255, verbose_name='\u041f\u043e\u044f\u0441\u043d\u0435\u043d\u0438\u044f', blank=True)),
                ('peoples', models.CommaSeparatedIntegerField(max_length=255, verbose_name='\u041a\u043e\u043b-\u0432\u043e \u043b\u044e\u0434\u0435\u0439', blank=True)),
                ('duration', models.CharField(max_length=255, verbose_name='\u041f\u0440\u043e\u0434\u043e\u043b\u0436\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c', blank=True)),
                ('expiration_date', models.DateField(null=True, verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0434\u043e', blank=True)),
                ('type', models.IntegerField(default=0, verbose_name='\u0422\u0438\u043f', choices=[(0, '\u0443\u0441\u043b\u0443\u0433\u0430'), (1, '\u0442\u043e\u0432\u0430\u0440')])),
                ('article', models.PositiveIntegerField(unique=True, verbose_name='\u0410\u0440\u0442\u0438\u043a\u0443\u043b')),
                ('tags', models.TextField(verbose_name='\u0422\u0435\u0433\u0438', blank=True)),
                ('active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u044b\u0439')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': '\u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Partner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('password', models.CharField(max_length=10, verbose_name='\u041f\u0430\u0440\u043e\u043b\u044c')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': '\u043f\u0430\u0440\u0442\u043d\u0435\u0440',
                'verbose_name_plural': '\u043f\u0430\u0440\u0442\u043d\u0435\u0440\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='entertainment',
            name='partner',
            field=models.ForeignKey(related_name='items', verbose_name='\u041f\u0430\u0440\u0442\u043d\u0435\u0440', blank=True, to='store.Partner'),
            preserve_default=True,
        ),
        migrations.AlterModelOptions(
            name='orderitem',
            options={'verbose_name': '\u0435\u0434\u0438\u043d\u0438\u0446\u0430 \u0437\u0430\u043a\u0430\u0437\u0430', 'verbose_name_plural': '\u0435\u0434\u0438\u043d\u0438\u0446\u044b \u0437\u0430\u043a\u0430\u0437\u0430'},
        ),
        migrations.RemoveField(
            model_name='orderitem',
            name='item',
        ),
        migrations.AddField(
            model_name='orderitem',
            name='conditions',
            field=models.CharField(max_length=255, verbose_name='\u0423\u0441\u043b\u043e\u0432\u0438\u044f', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderitem',
            name='date_redemption',
            field=models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u043f\u043e\u0433\u0430\u0448\u0435\u043d\u0438\u044f', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderitem',
            name='duration',
            field=models.CharField(max_length=255, verbose_name='\u041f\u0440\u043e\u0434\u043e\u043b\u0436\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderitem',
            name='expiration_date',
            field=models.DateField(null=True, verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0434\u043e', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderitem',
            name='explanations',
            field=models.CharField(max_length=255, verbose_name='\u041f\u043e\u044f\u0441\u043d\u0435\u043d\u0438\u044f', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderitem',
            name='name',
            field=models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderitem',
            name='partner',
            field=models.ForeignKey(related_name='ordered_items', to='store.Partner', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderitem',
            name='peoples',
            field=models.CommaSeparatedIntegerField(max_length=255, verbose_name='\u041a\u043e\u043b-\u0432\u043e \u043b\u044e\u0434\u0435\u0439', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderitem',
            name='revenue',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0421\u0435\u0431\u0435\u0441\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderitem',
            name='status',
            field=models.IntegerField(default=0, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(0, '\u043d\u043e\u0432\u044b\u0439'), (1, '\u0430\u043a\u0442\u0438\u0432\u043d\u044b\u0439'), (2, '\u043e\u043f\u043b\u0430\u0447\u0435\u043d'), (3, '\u043e\u0436\u0438\u0434\u0430\u0435\u0442 \u043e\u043f\u043b\u0430\u0442\u044b'), (4, '\u043f\u0440\u043e\u0441\u0440\u043e\u0447\u0435\u043d')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderitem',
            name='type',
            field=models.IntegerField(default=0, verbose_name='\u0422\u0438\u043f', choices=[(0, '\u0443\u0441\u043b\u0443\u0433\u0430'), (1, '\u0442\u043e\u0432\u0430\u0440')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='basketitem',
            name='item',
            field=models.ForeignKey(to='store.Entertainment'),
            preserve_default=True,
        ),
        migrations.DeleteModel(
            name='Entertaiment',
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-07-00-52-51', verbose_name=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.IntegerField(default=0, verbose_name='', choices=[(0, '\u043d\u043e\u0432\u044b\u0439'), (1, '\u043d\u0430 \u043e\u043f\u043b\u0430\u0442\u0443'), (2, '\u043e\u043f\u043b\u0430\u0447\u0435\u043d')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='price',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0426\u0435\u043d\u0430'),
            preserve_default=True,
        ),
    ]
