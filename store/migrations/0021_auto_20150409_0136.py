# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0020_auto_20150408_2216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entertainment',
            name='partner',
            field=models.ForeignKey(related_name='items', verbose_name='\u041f\u0430\u0440\u0442\u043d\u0435\u0440', blank=True, to='store.Partner', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entertainment',
            name='type',
            field=models.IntegerField(default=0, verbose_name='\u0422\u0438\u043f', choices=[(0, '\u0443\u0441\u043b\u0443\u0433\u0430'), (1, '\u0442\u043e\u0432\u0430\u0440'), (2, '\u043a\u0430\u0440\u0442\u0430')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-09-01-36-41', verbose_name=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='type',
            field=models.IntegerField(default=0, verbose_name='\u0422\u0438\u043f', choices=[(0, '\u0443\u0441\u043b\u0443\u0433\u0430'), (1, '\u0442\u043e\u0432\u0430\u0440'), (2, '\u043a\u0430\u0440\u0442\u0430')]),
            preserve_default=True,
        ),
    ]
