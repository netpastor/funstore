# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0018_auto_20150408_1346'),
    ]

    operations = [
        migrations.CreateModel(
            name='Card',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('login', models.CharField(max_length=15, verbose_name='\u043b\u043e\u0433\u0438\u043d')),
                ('password', models.CharField(max_length=15, verbose_name='\u043f\u0430\u0440\u043e\u043b\u044c')),
                ('summa', models.PositiveIntegerField(default=0, verbose_name='\u0441\u0443\u043c\u043c\u0430')),
                ('date_expired', models.DateField(verbose_name='\u0434\u0430\u0442\u0430 \u0433\u043e\u0434\u043d\u043e\u0441\u0442\u0438')),
                ('status', models.PositiveIntegerField(default=0, verbose_name='\u0441\u0442\u0430\u0442\u0443\u0441', choices=[(0, '\u0430\u043a\u0442\u0438\u0432\u043d\u0430\u044f'), (1, '\u043d\u0435\u0430\u043a\u0442\u0438\u0432\u043d\u0430\u044f'), (2, '\u043f\u0440\u043e\u0441\u0440\u043e\u0447\u0435\u043d\u0430')])),
                ('blocked', models.BooleanField(default=False, verbose_name='\u0437\u0430\u0431\u043b\u043e\u043a\u0438\u0440\u043e\u0432\u0430\u043d\u0430')),
            ],
            options={
                'verbose_name': '\u043f\u043e\u0434\u0430\u0440\u043e\u0447\u043d\u0430\u044f \u043a\u0430\u0440\u0442\u0430',
                'verbose_name_plural': '\u043f\u043e\u0434\u0430\u0440\u043e\u0447\u043d\u044b\u0435 \u043a\u0430\u0440\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CardTransaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('summa', models.PositiveIntegerField(verbose_name='\u0441\u0443\u043c\u043c\u0430')),
                ('date_operation', models.DateTimeField(auto_now_add=True, verbose_name='\u0434\u0430\u0442\u0430')),
                ('info', models.TextField(verbose_name='\u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f', blank=True)),
                ('card', models.ForeignKey(related_name='transactions', to='store.Card')),
            ],
            options={
                'verbose_name': '\u043e\u043f\u0435\u0440\u0430\u0446\u0438\u044f',
                'verbose_name_plural': '\u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-08-21-45-24', verbose_name=''),
            preserve_default=True,
        ),
    ]
