# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0028_auto_20150410_1222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cardtransaction',
            name='basket',
            field=models.ForeignKey(related_name='transactions', to='store.Basket', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-10-13-52-10', verbose_name=''),
            preserve_default=True,
        ),
    ]
