# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0023_auto_20150409_1141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-09-11-47-20', verbose_name=''),
            preserve_default=True,
        ),
    ]
