# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.CharField(max_length=255, serialize=False, verbose_name='id', primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('category_type', models.PositiveIntegerField(default=1, verbose_name='', choices=[(0, '\u043e\u0434\u043d\u043e\u0437\u043d\u0430\u0447\u043d\u044b\u0439 \u0432\u044b\u0431\u043e\u0440'), (1, '\u043c\u043d\u043e\u0436\u0435\u0441\u0442\u0432\u0435\u043d\u043d\u044b\u0439 \u0432\u044b\u0431\u043e\u0440')])),
            ],
            options={
                'verbose_name': '\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f',
                'verbose_name_plural': '\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CategoryItem',
            fields=[
                ('id', models.CharField(max_length=255, serialize=False, verbose_name='id', primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('active', models.BooleanField(default=True, verbose_name='')),
                ('parent', models.ForeignKey(related_name='items', to='store.Category')),
            ],
            options={
                'verbose_name': 'CategoryItem',
                'verbose_name_plural': 'CategoryItems',
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='EntertaimentAgeType',
        ),
        migrations.DeleteModel(
            name='EntertaimentGenderType',
        ),
        migrations.DeleteModel(
            name='EntertaimentLocation',
        ),
        migrations.DeleteModel(
            name='EntertaimentType',
        ),
        migrations.RemoveField(
            model_name='entertaiment',
            name='age',
        ),
        migrations.RemoveField(
            model_name='entertaiment',
            name='gender',
        ),
        migrations.RemoveField(
            model_name='entertaiment',
            name='location',
        ),
        migrations.RemoveField(
            model_name='entertaiment',
            name='type',
        ),
        migrations.AddField(
            model_name='entertaiment',
            name='tags',
            field=models.TextField(verbose_name='', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-01-19-01-35', verbose_name=''),
            preserve_default=True,
        ),
    ]
