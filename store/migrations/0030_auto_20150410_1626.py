# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0029_auto_20150410_1352'),
    ]

    operations = [
        migrations.AddField(
            model_name='cardtransaction',
            name='order',
            field=models.ForeignKey(related_name='transactions', to='store.Order', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-10-16-26-49', verbose_name=''),
            preserve_default=True,
        ),
    ]
