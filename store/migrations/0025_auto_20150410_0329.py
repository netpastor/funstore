# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0024_auto_20150409_1147'),
    ]

    operations = [
        migrations.AddField(
            model_name='cardtransaction',
            name='order',
            field=models.ForeignKey(related_name='transactions', to='store.Order', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cardtransaction',
            name='status',
            field=models.IntegerField(default=0, verbose_name='', choices=[(0, '\u043d\u043e\u0432\u0430\u044f'), (1, '\u043f\u0440\u043e\u0432\u0435\u0434\u0435\u043d\u0430')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-10-03-29-43', verbose_name=''),
            preserve_default=True,
        ),
    ]
