# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0025_auto_20150410_0329'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='card',
            name='order_item',
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-10-04-14-58', verbose_name=''),
            preserve_default=True,
        ),
    ]
