# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0011_auto_20150406_1027'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='siteconfiguration',
            options={'verbose_name': '\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0441\u0430\u0439\u0442\u0430', 'verbose_name_plural': '\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0441\u0430\u0439\u0442\u0430'},
        ),
        migrations.RemoveField(
            model_name='basketextaitem',
            name='item',
        ),
        migrations.RemoveField(
            model_name='orderextraitem',
            name='item',
        ),
        migrations.DeleteModel(
            name='ExtraService',
        ),
        migrations.AddField(
            model_name='basketextaitem',
            name='name',
            field=models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basketextaitem',
            name='price',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0446\u0435\u043d\u0430'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basketextaitem',
            name='type',
            field=models.IntegerField(default=0, verbose_name='\u0442\u0438\u043f', choices=[(0, '\u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderextraitem',
            name='name',
            field=models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderextraitem',
            name='type',
            field=models.IntegerField(default=0, verbose_name='\u0442\u0438\u043f', choices=[(0, '\u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-06-11-57-33', verbose_name=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='orderextraitem',
            name='price',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0446\u0435\u043d\u0430'),
            preserve_default=True,
        ),
    ]
