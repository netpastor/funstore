# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0030_auto_20150410_1626'),
    ]

    operations = [
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-13-12-54-17', verbose_name=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='partner',
            field=models.ForeignKey(related_name='ordered_items', verbose_name='\u041f\u0430\u0440\u0442\u043d\u0435\u0440', to='store.Partner', null=True),
            preserve_default=True,
        ),
    ]
