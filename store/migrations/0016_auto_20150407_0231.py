# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0015_auto_20150407_0228'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderitem',
            name='code',
            field=models.CharField(max_length=15, verbose_name='\u041a\u043e\u0434', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-07-02-31-18', verbose_name=''),
            preserve_default=True,
        ),
    ]
