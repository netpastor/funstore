# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0026_auto_20150410_0414'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderitem',
            name='card',
            field=models.ForeignKey(blank=True, to='store.Card', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-10-04-34-48', verbose_name=''),
            preserve_default=True,
        ),
    ]
