# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0027_auto_20150410_0434'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cardtransaction',
            name='order',
        ),
        migrations.AddField(
            model_name='cardtransaction',
            name='basket',
            field=models.ForeignKey(related_name='transactions', to='store.Order', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-10-12-22-03', verbose_name=''),
            preserve_default=True,
        ),
    ]
