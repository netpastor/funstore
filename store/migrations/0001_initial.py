# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Entertaiment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('article', models.PositiveIntegerField(unique=True, verbose_name='\u0410\u0440\u0442\u0438\u043a\u0443\u043b')),
                ('price', models.PositiveIntegerField(default=0, verbose_name='\u0426\u0435\u043d\u0430')),
                ('revenue', models.PositiveIntegerField(default=0, verbose_name='\u0421\u0435\u0431\u0435\u0441\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c')),
                ('conditions', models.CharField(max_length=255, verbose_name='\u0423\u0441\u043b\u043e\u0432\u0438\u044f', blank=True)),
                ('explanations', models.CharField(max_length=255, verbose_name='\u041f\u043e\u044f\u0441\u043d\u0435\u043d\u0438\u044f', blank=True)),
                ('peoples', models.CommaSeparatedIntegerField(max_length=255, verbose_name='\u041a\u043e\u043b-\u0432\u043e \u043b\u044e\u0434\u0435\u0439', blank=True)),
                ('duration', models.CharField(max_length=255, verbose_name='\u041f\u0440\u043e\u0434\u043e\u043b\u0436\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c', blank=True)),
                ('expiration_date', models.DateField(verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0434\u043e')),
                ('type', models.CharField(max_length=255, verbose_name='\u0422\u0438\u043f', blank=True)),
                ('gender', models.CharField(max_length=255, verbose_name='\u0414\u043b\u044f \u043a\u043e\u0433\u043e', blank=True)),
                ('age', models.CharField(max_length=255, verbose_name='\u0412\u043e\u0437\u0440\u0430\u0441\u0442', blank=True)),
                ('location', models.CharField(max_length=255, verbose_name='\u0420\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('active', models.BooleanField(default=True, verbose_name='')),
            ],
            options={
                'verbose_name': '\u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EntertaimentAgeType',
            fields=[
                ('id', models.CharField(max_length=255, serialize=False, verbose_name='id', primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0432\u043e\u0437\u0440\u0430\u0441\u0442 \u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u044f',
                'verbose_name_plural': '\u0432\u043e\u0437\u0440\u0430\u0441\u0442\u0430 \u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EntertaimentGenderType',
            fields=[
                ('id', models.CharField(max_length=255, serialize=False, verbose_name='id', primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u043f\u043e\u043b \u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u044f',
                'verbose_name_plural': '\u043f\u043e\u043b\u0430 \u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EntertaimentLocation',
            fields=[
                ('id', models.CharField(max_length=255, serialize=False, verbose_name='id', primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0440\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u0435 \u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u044f',
                'verbose_name_plural': '\u0440\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u044f \u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EntertaimentType',
            fields=[
                ('id', models.CharField(max_length=255, serialize=False, verbose_name='id', primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0442\u0438\u043f \u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u044f',
                'verbose_name_plural': '\u0442\u0438\u043f\u044b \u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Import',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('csv_file', models.FileField(upload_to=b'cvs/2015-03-31-20-53-11', verbose_name='')),
                ('import_date', models.DateTimeField(auto_now_add=True, verbose_name='')),
            ],
            options={
                'verbose_name': '\u0438\u043c\u043f\u043e\u0440\u0442',
                'verbose_name_plural': '\u0438\u043c\u043f\u043e\u0440\u0442\u044b',
            },
            bases=(models.Model,),
        ),
    ]
