# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0017_auto_20150408_1317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-08-13-46-47', verbose_name=''),
            preserve_default=True,
        ),
    ]
