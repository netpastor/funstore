# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0006_auto_20150402_1304'),
    ]

    operations = [
        migrations.RenameField(
            model_name='basket',
            old_name='session_d',
            new_name='session_id',
        ),
        migrations.AlterField(
            model_name='basketitem',
            name='count',
            field=models.PositiveIntegerField(default=1, verbose_name=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-02-15-39-22', verbose_name=''),
            preserve_default=True,
        ),
    ]
