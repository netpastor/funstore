# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0004_auto_20150401_1950'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='category_type',
            field=models.PositiveIntegerField(default=1, verbose_name='\u0442\u0438\u043f \u0432\u044b\u0431\u043e\u0440\u0430', choices=[(0, '\u043e\u0434\u043d\u043e\u0437\u043d\u0430\u0447\u043d\u044b\u0439 \u0432\u044b\u0431\u043e\u0440'), (1, '\u043c\u043d\u043e\u0436\u0435\u0441\u0442\u0432\u0435\u043d\u043d\u044b\u0439 \u0432\u044b\u0431\u043e\u0440')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-01-21-07-55', verbose_name=''),
            preserve_default=True,
        ),
    ]
