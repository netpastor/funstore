# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0002_auto_20150401_1901'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='active',
            field=models.BooleanField(default=True, verbose_name=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-01-19-08-35', verbose_name=''),
            preserve_default=True,
        ),
    ]
