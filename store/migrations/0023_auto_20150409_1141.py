# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0022_auto_20150409_0254'),
    ]

    operations = [
        migrations.RenameField(
            model_name='card',
            old_name='summa',
            new_name='nominal',
        ),
        migrations.AddField(
            model_name='card',
            name='order_item',
            field=models.OneToOneField(related_name='card', null=True, blank=True, to='store.OrderItem'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='siteconfiguration',
            name='card_days_expired',
            field=models.IntegerField(default=31, verbose_name='\u0421\u0440\u043e\u043a \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044f \u043a\u0430\u0440\u0442\u044b, \u0434\u043d\u0435\u0439'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='basketitem',
            name='count',
            field=models.PositiveIntegerField(default=1, verbose_name='\u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-09-11-41-18', verbose_name=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='created',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0441\u043e\u0437\u0434\u0430\u043d'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.IntegerField(default=0, verbose_name='\u0441\u0442\u0430\u0442\u0443\u0441', choices=[(0, '\u043d\u043e\u0432\u044b\u0439'), (1, '\u043d\u0430 \u043e\u043f\u043b\u0430\u0442\u0443'), (2, '\u043e\u043f\u043b\u0430\u0447\u0435\u043d')]),
            preserve_default=True,
        ),
    ]
