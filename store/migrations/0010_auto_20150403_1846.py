# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0009_auto_20150403_0955'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=255, verbose_name='\u0438\u043c\u044f')),
                ('phone', models.CharField(max_length=12, verbose_name='\u0442\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('email', models.EmailField(max_length=75, verbose_name='email')),
                ('status', models.IntegerField(default=0, verbose_name='', choices=[(0, '\u043d\u043e\u0432\u044b\u0439'), (1, '\u043f\u0440\u043e\u0432\u0435\u0440\u0435\u043d'), (2, '\u043e\u043f\u043b\u0430\u0447\u0435\u043d')])),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='')),
            ],
            options={
                'verbose_name': '\u0437\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0437\u0430\u043a\u0430\u0437\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrderExtraItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.PositiveIntegerField(verbose_name='')),
                ('item', models.ForeignKey(to='store.ExtraService')),
                ('order', models.ForeignKey(related_name='extra_items', to='store.Order')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.PositiveIntegerField(verbose_name='')),
                ('item', models.ForeignKey(to='store.Entertaiment')),
                ('order', models.ForeignKey(related_name='items', to='store.Order')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-03-18-46-27', verbose_name=''),
            preserve_default=True,
        ),
    ]
