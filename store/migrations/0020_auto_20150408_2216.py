# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0019_auto_20150408_2145'),
    ]

    operations = [
        migrations.AddField(
            model_name='entertainment',
            name='need_delivery',
            field=models.BooleanField(default=False, verbose_name='\u043d\u0443\u0436\u043d\u0430 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='categoryitem',
            name='parent',
            field=models.ForeignKey(related_name='items', blank=True, to='store.Category', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-08-22-16-36', verbose_name=''),
            preserve_default=True,
        ),
    ]
