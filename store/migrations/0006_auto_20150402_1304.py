# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0005_auto_20150401_2107'),
    ]

    operations = [
        migrations.CreateModel(
            name='Basket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('session_d', models.CharField(max_length=255, verbose_name='\u041a\u043e\u0434 \u0441\u0435\u0441\u0441\u0438\u0438')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d\u0430')),
                ('modified', models.DateTimeField(auto_now_add=True, verbose_name='\u0418\u0437\u043c\u0435\u043d\u0435\u043d\u0430')),
                ('status', models.IntegerField(default=0, verbose_name='\u0441\u0442\u0430\u0442\u0443\u0441', choices=[(0, '\u043e\u0442\u043a\u0440\u044b\u0442\u0430'), (1, '\u0437\u0430\u043a\u0440\u044b\u0442\u0430')])),
            ],
            options={
                'ordering': ['modified'],
                'verbose_name': '\u043a\u043e\u0440\u0437\u0438\u043d\u0430',
                'verbose_name_plural': '\u043a\u043e\u0440\u0437\u0438\u043d\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BasketExtaItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('basket', models.ForeignKey(related_name='extra_items', to='store.Basket')),
            ],
            options={
                'verbose_name': '\u0434\u043e\u043f.\u044d\u043b\u0435\u043c\u0435\u043d\u0442 \u043a\u043e\u0440\u0437\u0438\u043d\u044b',
                'verbose_name_plural': '\u0434\u043e\u043f.\u044d\u043b\u0435\u043c\u0435\u043d\u0442\u044b \u043a\u043e\u0440\u0437\u0438\u043d\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BasketItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.PositiveIntegerField(verbose_name='')),
                ('basket', models.ForeignKey(related_name='items', to='store.Basket')),
                ('item', models.ForeignKey(to='store.Entertaiment')),
            ],
            options={
                'verbose_name': '\u044d\u043b\u0435\u043c\u0435\u043d\u0442 \u043a\u043e\u0440\u0437\u0438\u043d\u044b',
                'verbose_name_plural': '\u044d\u043b\u0435\u043c\u0435\u043d\u0442\u044b \u043a\u043e\u0440\u0437\u0438\u043d\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExtraService',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('price', models.PositiveIntegerField(default=0, verbose_name='\u0426\u0435\u043d\u0430')),
            ],
            options={
                'verbose_name': '\u0434\u043e\u043f.\u0443\u0441\u043b\u0443\u0433\u0430',
                'verbose_name_plural': '\u0434\u043e\u043f.\u0443\u0441\u043b\u0443\u0433\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='basketextaitem',
            name='item',
            field=models.ForeignKey(to='store.ExtraService'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='entertaiment',
            name='type',
            field=models.IntegerField(default=0, verbose_name='\u0422\u0438\u043f', choices=[(0, '\u0443\u0441\u043b\u0443\u0433\u0430'), (1, '\u0442\u043e\u0432\u0430\u0440')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entertaiment',
            name='active',
            field=models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u044b\u0439'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entertaiment',
            name='tags',
            field=models.TextField(verbose_name='\u0422\u0435\u0433\u0438', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-02-13-04-08', verbose_name=''),
            preserve_default=True,
        ),
    ]
