# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0012_auto_20150406_1157'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='entertaiment',
            options={'ordering': ['name'], 'verbose_name': '\u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u0435', 'verbose_name_plural': '\u0440\u0430\u0437\u0432\u043b\u0435\u0447\u0435\u043d\u0438\u044f'},
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-06-12-32-11', verbose_name=''),
            preserve_default=True,
        ),
    ]
