# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0033_auto_20150413_2021'),
    ]

    operations = [
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-13-21-16-08', verbose_name=''),
            preserve_default=True,
        ),
    ]
