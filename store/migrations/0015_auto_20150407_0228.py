# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0014_auto_20150407_0052'),
    ]

    operations = [
        migrations.RenameField(
            model_name='entertainment',
            old_name='expiration_date',
            new_name='date_expiration',
        ),
        migrations.RenameField(
            model_name='orderitem',
            old_name='expiration_date',
            new_name='date_expiration',
        ),
        migrations.AlterField(
            model_name='import',
            name='csv_file',
            field=models.FileField(upload_to=b'cvs/2015-04-07-02-28-10', verbose_name=''),
            preserve_default=True,
        ),
    ]
