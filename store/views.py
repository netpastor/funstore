# -*- coding:utf-8 -*-
import datetime

from django import http
from django.conf import settings
from django.contrib import messages
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Sum
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.template.loader import get_template
from django.template import Context
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, RedirectView, UpdateView, DeleteView, TemplateView, FormView
import xhtml2pdf.pisa as pisa

from .models import Entertainment, CategoryItem, CATEGORY_ONLY, Basket, Order, \
    BasketItem, BASKET_OPEN, ORDER_NEW, PRODUCT, BasketExtaItem, DELIVERY, SiteConfiguration, OrderItem, \
    ORDER_ITEM_PAYED, ORDER_PAYED, CARD, SERVICE, Card, TRANSACTION_ACCEPTED, CardTransaction, ORDER_ITEM_STATUS, \
    Partner
from .forms import BasketForm, BasketItemFormSet, OrderCreateForm, CardForm

try:
    import StringIO
    StringIO = StringIO.StringIO
except Exception:
    from io import StringIO

import cgi
import hashlib
import logging

log = logging.getLogger(__name__)


def render_to_pdf(template_src, context_dict):

    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    result = StringIO()
    pdf = pisa.pisaDocument(StringIO(html.encode("UTF-8")), result, encoding='UTF-8')
    if not pdf.err:
        return http.HttpResponse(result.getvalue(), content_type='application/pdf')
    return http.HttpResponse('We had some errors<pre>%s</pre>' % cgi.escape(html))


def get_basket(request):
    if not request.session.get('basket_id'):
        return
    else:
        try:
            return Basket.objects.get(
                status=BASKET_OPEN, id=request.session.get('basket_id'))
        except Basket.DoesNotExist:
            return


def get_basket_info(request):
    basket = get_basket(request)
    if basket:
        return basket.item_count, basket.summa


def get_or_create_basket(request):
    basket = get_basket(request)
    if not basket:
        basket = Basket.objects.create(session_id=request.session.session_key)
        request.session['basket_id'] = basket.id
    return basket


def create_signature(*args, **kwargs):
    sign_string = '{MNT_ID}{MNT_TRANSACTION_ID}{MNT_AMOUNT}{MNT_CURRENCY_CODE}{MNT_SUBSCRIBER_ID}{MNT_TEST_MODE}{MNT_CHECK_CODE}'.format(**kwargs)
    return hashlib.md5(sign_string).hexdigest()


def make_order_payed(order):
    order.status = ORDER_PAYED
    order.save()

    for order_item in order.items.all():
        order_item.status = ORDER_ITEM_PAYED
        order_item.generate_number()
        if order_item.type == CARD:
            card = Card.create_card(order_item.price)
            order_item.card = card
        order_item.save()

    for transaction in order.transactions.all():
        transaction.status = TRANSACTION_ACCEPTED
        card = transaction.card
        card.blocked = False
        card.save()
        transaction.save()


class AddToBasketView(RedirectView):

    def post(self, request, *args, **kwargs):
        basket = get_or_create_basket(request)
        item = get_object_or_404(Entertainment, id=request.POST.get('cart_item'))

        basket_item, created = BasketItem.objects.get_or_create(basket=basket, item=item)
        if not created:
            basket_item.count += 1
            basket_item.save()

        if basket_item.item.type == PRODUCT and not basket.has_extra_service(DELIVERY):
            BasketExtaItem.objects.create(
                basket=basket,
                type=DELIVERY,
                name='Доставка',
                price=SiteConfiguration.get_solo().price_delivery
            )

        return redirect(request.META['HTTP_REFERER'])

class OrderItemsView(ListView):
    template_name = 'order_items.html'
    model = OrderItem
    context_object_name = 'orderitems_list'

    def filter_queryset(self, qs):
        statuses = self.request.GET.get('status')
        if statuses:
            qs = qs.filter(status__in=list(statuses.split(',')))
        partners = self.request.GET.get('partner')
        if partners:
            qs = qs.filter(partner__in=list(partners.split(',')))
        return qs

    def get_context_data(self, **kwargs):

        context = super(OrderItemsView, self).get_context_data(**kwargs)
        self.queryset = self.filter_queryset(super(OrderItemsView, self).get_queryset())

        price_total = self.queryset.aggregate(Sum('price')).items()[0][1]
        revenue_total = self.queryset.aggregate(Sum('revenue')).items()[0][1]

        context['status'] = ORDER_ITEM_STATUS
        context['partners'] = [(p.id, p.name) for p in Partner.objects.all()]
        context['selected_statuses'] = [int(id) for id in self.request.GET.get('status').split(',')] if self.request.GET.get('status') else None
        context['selected_partners'] = [int(id) for id in self.request.GET.get('partner').split(',')] if self.request.GET.get('partner') else None
        context['price_total'] = price_total if price_total else 0
        context['revenue_total'] = revenue_total if revenue_total else 0
        context['orderitems_list'] = self.queryset
        return context


class EntertainmentView(ListView):

    template_name = 'index.html'
    context_object_name = 'entertainment_list'
    model = Entertainment

    def filter_queryset(self, qs):
        values = self.request.GET.get('tags')
        if values:
            for tag_id in values.split(','):
                try:
                    CategoryItem.objects.get(id=tag_id, active=True)
                    qs = qs.filter(tags__contains=tag_id)
                except CategoryItem.DoesNotExist:
                    pass
        return qs

    def if_filter_active(self, tag_id):
        values = self.request.GET.get('tags')
        if values:
            if tag_id in values:
                return True
        return False

    def get_context_data(self, **kwargs):

        context = super(EntertainmentView, self).get_context_data(**kwargs)
        queryset = super(EntertainmentView, self).get_queryset().filter(active=True)

        queryset = self.filter_queryset(queryset)

        filters = []
        active_filters = []

        for cat_item in CategoryItem.objects.filter(active=True):

            only_check = True
            if cat_item.parent.category_type == CATEGORY_ONLY and self.request.GET.get('tags'):
                for prnt_item in cat_item.parent.items.exclude(id=cat_item.id):
                    if prnt_item.id in self.request.GET.get('tags'):
                        only_check = False

            if only_check:
                item_count = queryset.filter(
                    tags__contains=cat_item.id).count()

                if item_count:
                    if self.if_filter_active(cat_item.id):
                        array = active_filters
                    else:
                        array = filters
                    array.append({
                        'count': item_count,
                        'name': cat_item.name,
                        'id': cat_item.id
                    })

        context['entertainment_list'] = queryset
        context['basket_info'] = get_basket_info(self.request)
        context['filters'] = sorted(
            filters,
            key=lambda key_value: key_value['count'],
            reverse=True
        )
        context['active_filters'] = sorted(
            active_filters,
            key=lambda key_value: key_value['count'],
            reverse=True
        )
        return context


class BasketView(UpdateView):

    model = Basket
    form_class = BasketForm
    second_form_class = OrderCreateForm
    success_url = reverse_lazy('basket')
    success_order_url = reverse_lazy('confirm')

    def post(self, request, *args, **kwargs):

        self.object = self.get_object()

        if 'basket_form' in request.POST:
            form = self.get_form(self.form_class)
            basket_item_form = BasketItemFormSet(request.POST, instance=self.object)

            if form.is_valid() and basket_item_form.is_valid():
                basket_item_form.instance = self.object
                basket_item_form.save()

            context_dict = {
                'basket_form': form,
                'basket_item_form': basket_item_form,
                'card_form': CardForm()
            }

        elif 'order_form' in request.POST:
            form = OrderCreateForm(request.POST)

            if form.is_valid():
                order = form.save(self.request)
                if order.summa_to_pay:
                    return redirect(self.success_order_url)
                else:
                    make_order_payed(order)
                    return HttpResponseRedirect(reverse('payment_success')+'?MNT_TRANSACTION_ID={}'.format(order.id))
            else:
                basket_item_form = BasketItemFormSet(instance=self.get_object())

            context_dict = {
                'basket_form': form,
                'basket_item_form': basket_item_form,
                'card_form': CardForm()
            }

        elif 'card_form' in request.POST:
            form = CardForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                if not Card.objects.filter(login=cd['login'], password=cd['password']):
                    messages.add_message(request, messages.ERROR, u'Карты с такими данными не найдено')
                else:
                    card = Card.objects.get(login=cd['login'], password=cd['password'])
                    if card.blocked:
                        messages.add_message(request, messages.ERROR, u'Карты заблокирована для транзакции')
                    elif card.date_expired < datetime.date.today():
                         messages.add_message(request, messages.ERROR, u'Карта просрочена')
                    elif not card.balance:
                        messages.add_message(request, messages.ERROR, u'Карты использована полностью')
                    else:
                        self.object.add_card_transaction(card)
                        form = CardForm()

            context_dict = {
                'basket_form': BasketForm(instance=self.get_object()),
                'basket_item_form': BasketItemFormSet(instance=self.get_object()),
                'card_form': form
            }

        return self.render_to_response(
            self.get_context_data(**context_dict)
        )

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return self.render_to_response(
            self.get_context_data(
                basket_form=BasketForm(instance=self.get_object()),
                basket_item_form=BasketItemFormSet(instance=self.get_object()),
                order_form=OrderCreateForm(),
                card_form=CardForm()
                )
        )

    def get_object(self, queryset=None):
        return get_or_create_basket(self.request)

    def get_context_data(self, **kwargs):
        if 'basket_info' not in kwargs:
            kwargs['basket_info'] = get_basket_info(self.request)
        if 'object' not in kwargs:
            kwargs['object'] = self.get_object()
        kwargs['basket_extra_items'] = get_basket(self.request).extra_items.all()
        return kwargs


class ConfirmView(FormView):

    template_name = 'confirm.html'
    form_class = CardForm

    def get_order(self, request):
        try:
            return Order.objects.get(
                status=ORDER_NEW,
                id=self.request.session.get('order_id')
            )
        except Order.DoesNotExist:
            raise Http404

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        order = self.get_order(request)
        if form.is_valid():
            cd = form.cleaned_data
            if not Card.objects.filter(login=cd['login'], password=cd['password']):
                messages.add_message(request, messages.ERROR, u'Карты с такими данными не найдено')
            else:
                card = Card.objects.get(login=cd['login'], password=cd['password'])
                if card.blocked:
                    messages.add_message(request, messages.ERROR, u'Карты заблокирована для транзакции')
                elif card.date_expired < datetime.date.today():
                     messages.add_message(request, messages.ERROR, u'Карта просрочена')
                elif not card.balance:
                    messages.add_message(request, messages.ERROR, u'Карты использована полностью')
                else:
                    order.add_card_transaction(card)
                    form = self.form_class()
                    if not order.summa_to_pay:
                        make_order_payed(order)
                        return HttpResponseRedirect(reverse('payment_success')+'?MNT_TRANSACTION_ID={}'.format(order.id))

        return self.render_to_response(
            self.get_context_data(
                **{
                    'card_form': form,
                    'order': order,
                }
            )
        )

    def get_context_data(self, **kwargs):
        order = self.get_order(self.request)
        if 'order' not in kwargs:
            kwargs['order'] = order

        kwargs['PAYANYWAY_ACCOUNT'] = settings.PAYANYWAY_ACCOUNT

        sign_dict = {
            'MNT_ID': settings.PAYANYWAY_ACCOUNT,
            'MNT_TRANSACTION_ID': order.id,
            'MNT_AMOUNT': '{}.00'.format(order.summa_to_pay),
            'MNT_CURRENCY_CODE': 'RUB',
            'MNT_SUBSCRIBER_ID': '',
            'MNT_TEST_MODE': 1,
            'MNT_CHECK_CODE': settings.PAYANYWAY_CHECKCODE
        }
        kwargs['signature'] = create_signature(**sign_dict)
        return kwargs


class DelFromBasketView(DeleteView):
    model = BasketItem

    def post(self, request, *args, **kwargs):
        if request.POST['type'] == 'basket':
            basket_item = get_object_or_404(BasketItem, id=request.POST.get('id'))
            basket = get_basket(self.request)
            if basket_item.item.type == PRODUCT:
                if basket.items.filter(item__type=PRODUCT).count() == 1:
                    try:
                        BasketExtaItem.objects.get(basket=basket, type=DELIVERY).delete()
                    except BasketExtaItem.DoesNotExist:
                        pass
            basket_item.delete()
        elif request.POST['type'] == 'transaction':
            trans = get_object_or_404(CardTransaction,id=request.POST.get('id'))
            card = trans.card
            card.blocked = False
            card.save()
            trans.delete()

        return redirect(reverse('basket'))

    def get_queryset(self):
        return self.model._default_manager.all()


class PDFView(TemplateView):
    def get_context_data(self, code, **kwargs):
        order_item = get_object_or_404(OrderItem, code=code)
        if order_item.type in (SERVICE, PRODUCT):
            self.template_name = 'service_pdf.html'
        elif order_item.type == CARD:
            self.template_name = 'card_pdf.html'
            kwargs['card'] = order_item.card
        kwargs['item'] = order_item
        kwargs['STATIC_ROOT'] = settings.STATIC_ROOT
        return kwargs

    def render_to_response(self, context, **response_kwargs):
         return render_to_pdf(self.template_name, context)


class PaymentSuccessView(TemplateView):
    template_name = 'payment_success.html'

    def get(self, request, *args, **kwargs):
        log.info('request.GET %s' % request.GET)
        log.info('request.POST %s' % request.POST)
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        log.info('request.GET %s' % request.GET)
        log.info('request.POST %s' % request.POST)

    def get_context_data(self, **kwargs):
        order = get_object_or_404(Order, id=self.request.GET.get('MNT_TRANSACTION_ID'))
        if order.status == ORDER_PAYED:
            kwargs['order'] = order
        return kwargs



class PaymentFailureView(TemplateView):
    template_name = 'payment_failure.html'

    def get(self, request, *args, **kwargs):
        log.info('request.GET %s' % request.GET)
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        log.info('request.GET %s' % request.GET)
        log.info('request.POST %s' % request.POST)


class PayUrlView(TemplateView):
    template_name = 'pay_url.html'

    def get(self, request, *args, **kwargs):
        log.info('request.GET %s' % request.GET)
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        log.info('request.POST %s' % request.POST)

        order = get_object_or_404(Order, id=request.POST['MNT_TRANSACTION_ID'])
        make_order_payed(order)

        return http.HttpResponse('SUCCESS')

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PayUrlView, self).dispatch(*args, **kwargs)