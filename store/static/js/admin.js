(function($) {
    $(document).ready(function($) {
        $('#result_list tr:last').css({ backgroundColor: "#417690", fontWeight: "bolder", color: "white" });
        $('#result_list tr:last td.action-checkbox input').hide();
        $('#result_list tr:last td.field-name input').hide();
        $('#result_list tr:last th.field-name').text('Итого');
        $('#result_list tr:last td.field-status').text('');
        $('#result_list tr:last td.field-partner').text('');
        $('#result_list tr:last td.field-created_date').text('');
    });
})(django.jQuery);
