$(document).ready(function(){
    function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
            }
        }
    });

	$("li[role|='presentation']>a").bind( "click", function() {
		if ($(this).parent().hasClass('active')) {
			$(this).parent().removeClass('active')
		} else {
			$(this).parent().addClass('active')
		}

		var params = '';

		$("ul.nav-pills>li").each(function() {
			if ($(this).hasClass('active')) {
				params = params + $(this).find('a').attr('data-id') + ','
			}
		})
		location.assign($(location).attr('origin')+'/?tags='+params)

	})

    $(".btn.remove").bind( "click", function() {
        $.post( "/del_from_basket", { id: $(this).attr('data-id'), type: $(this).attr('data-type')} ).done(function() {
            location.reload(true);
        });
    })
})