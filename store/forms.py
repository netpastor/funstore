# -*- coding:utf-8 -*-
from django import forms
from django.forms import ModelForm
from django.forms.models import inlineformset_factory

from .models import Basket, BasketItem, Order, OrderItem, OrderExtraItem, BASKET_CLOSED


class BasketForm(ModelForm):
    class Meta:
        model = Basket
        fields = ['session_id',]


BasketItemFormSet = inlineformset_factory(Basket, BasketItem, extra=0)


class CardForm(forms.Form):
    login = forms.CharField(required=True, label=u'Логин карты')
    password = forms.CharField(required=True, label=u'Пароль карты', widget=forms.PasswordInput())


class OrderCreateForm(forms.Form):
    username = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    phone = forms.CharField(required=False)

    def save(self, request):
        cd = self.cleaned_data
        # try:
        basket = Basket.objects.get(id=request.session.get('basket_id'))
        # with transaction.atomic():
        order = Order(
            username=cd['username'],
            email=cd['email'],
            phone=cd['phone']
        )
        order.save()
        for basket_item in basket.items.all():
            for _ in range(basket_item.count):
                OrderItem.objects.create(
                    order=order,
                    name=basket_item.item.name,
                    price=basket_item.item.price,
                    revenue=basket_item.item.revenue,
                    conditions=basket_item.item.conditions,
                    explanations=basket_item.item.explanations,
                    peoples=basket_item.item.peoples,
                    duration=basket_item.item.duration,
                    date_expiration=basket_item.item.date_expiration,
                    partner=basket_item.item.partner,
                    type=basket_item.item.type
                )

        for extra_item in basket.extra_items.all():
            OrderExtraItem.objects.create(
                order=order,
                type=extra_item.type,
                name=extra_item.name,
                price=extra_item.price
            )

        for transaction in basket.transactions.all():
            transaction.order = order
            transaction.save()

        basket.status = BASKET_CLOSED
        basket.save()
        request.session['order_id'] = order.id

        return order