# -*- coding:utf-8 -*-
import datetime

from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.db.models import Sum

from solo.admin import SingletonModelAdmin

from models import (Entertainment, Category, CategoryItem, Import, Order, OrderItem, SiteConfiguration, Basket,
                    BasketItem, Partner, BasketExtaItem, OrderExtraItem, Card, CardTransaction, ORDER_ITEM_REPAYED)


class CardTransactionInline(admin.TabularInline):
    model = CardTransaction
    extra = 0
    max_num = 0


class CategoryItemInline(admin.TabularInline):
    model = CategoryItem
    extra = 0


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )
    inlines = [
        CategoryItemInline,
    ]


class BasketItemInline(admin.TabularInline):
    model = BasketItem
    exta = 0
    max_num = 0


class BasketExtraItemInline(admin.TabularInline):
    model = BasketExtaItem
    exta = 0
    max_num = 0


class BasketAdmin(admin.ModelAdmin):
    list_display = ('session_id', 'status', 'created', 'summa')
    inlines = [
        BasketItemInline,
        BasketExtraItemInline,
        CardTransactionInline
    ]


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    extra = 0
    max_num = 0


class OrderExtraItemInline(admin.TabularInline):
    model = OrderExtraItem
    extra = 0
    max_num = 0


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'email', 'status', 'created')
    list_filter = ('status', )

    inlines = [
        OrderItemInline,
        OrderExtraItemInline
    ]


class TotalSumChangeList(ChangeList):
    fields_to_total = ['price', ]

    def get_total_values(self, queryset):
        total = OrderItem()
        total.custom_alias_name = u"Итого"
        total.name = u"Итого"
        for field in self.fields_to_total:
            setattr(total, field, queryset.aggregate(Sum(field)).items()[0][1])
        return total

    def get_results(self, request):
        super(TotalSumChangeList, self).get_results(request)
        total = self.get_total_values(self.query_set)
        len(self.result_list)
        self.result_list._result_cache.append(total)


def make_repayed(modeladmin, request, queryset):
    queryset.update(status=ORDER_ITEM_REPAYED, date_redemption=datetime.datetime.now())
make_repayed.short_description = u"Отметить как погашенные"


class OrderItemAdmin(admin.ModelAdmin):

    class Media:
        js = ('js/admin.js',)

    list_display = ('name', 'price', 'status', 'partner', 'created_date')
    list_filter = ('status', 'partner')
    actions = [make_repayed]

    def created_date(self, obj):
        return obj.order.created
    created_date.short_description = u'Дата покупки'
    created_date.admin_order_field = u'order__created'

    def get_changelist(self, request, **kwargs):
        return TotalSumChangeList


class CardAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'date_expired', 'nominal', 'balance')
    list_filter = ('status', )

    inlines = [
        CardTransactionInline,
    ]


class EntertainmentAdmin(admin.ModelAdmin):
    list_display = ('article', 'name', 'type', 'price')
    list_filter = ('type',)

admin_models = (
    (Import, None),
    (Entertainment, EntertainmentAdmin),
    (Basket, BasketAdmin),
    (Category, CategoryAdmin),
    (Order, OrderAdmin),
    (SiteConfiguration, SingletonModelAdmin),
    (OrderItem, OrderItemAdmin),
    (Partner, None),
    (Card, CardAdmin)
)

for model, model_admin in admin_models:
    admin.site.register(model, model_admin)