# -*- coding:utf-8 -*-
from datetime import datetime, date, timedelta
import csv
import random
import uuid

from django.db import models
from django.db.models import Sum
from django.db.models.loading import get_model
from django.db.models.signals import post_save
from solo.models import SingletonModel


def _generate_code(code_length):
    allowed_chars = '123456789ABCDEFGHJKLNPRSTXY'
    return ''.join([random.choice(allowed_chars) for _ in range(code_length)])


class Partner(models.Model):
    class Meta:
        verbose_name = u'партнер'
        verbose_name_plural = u'партнеры'
        ordering = ['name', ]

    name = models.CharField(u'Название', max_length=255)
    password = models.CharField(u'Пароль', max_length=10)

    def __unicode__(self):
        return self.name

SERVICE, PRODUCT, CARD = range(3)
ENTERTAINMENT_TYPE = (
    (SERVICE, u'услуга'),
    (PRODUCT, u'товар'),
    (CARD, u'карта'),
)


class EntertainmentBase(models.Model):
    class Meta:
        abstract = True

    name = models.CharField(u'Название', max_length=255, blank=True)
    price = models.PositiveIntegerField(u'Цена', default=0)
    revenue = models.PositiveIntegerField(u'Себестоимость', default=0)
    conditions = models.CharField(u'Условия', max_length=255, blank=True)
    explanations = models.CharField(u'Пояснения', max_length=255, blank=True)
    peoples = models.CommaSeparatedIntegerField(
        u'Кол-во людей', max_length=255, blank=True)
    duration = models.CharField(
        u'Продолжительность', max_length=255, blank=True)
    date_expiration = models.DateField(u'Использовать до', blank=True, null=True)
    type = models.IntegerField(
        u'Тип', choices=ENTERTAINMENT_TYPE, default=SERVICE)


class Entertainment(EntertainmentBase):
    class Meta:
        verbose_name = u"развлечение"
        verbose_name_plural = u"развлечения"
        ordering = ['name', ]

    partner = models.ForeignKey(Partner, related_name='items', verbose_name=u'Партнер', blank=True, null=True)
    article = models.PositiveIntegerField(u'Артикул', unique=True)
    tags = models.TextField(u'Теги', blank=True)
    active = models.BooleanField(u'Активный', default=True)
    need_delivery = models.BooleanField(u'нужна доставка', default=False)

    def __unicode__(self):
        return self.name

    @property
    def tag_list(self):
        tags = []
        for tag_id in self.tags.split(','):
            try:
                tags.append(CategoryItem.objects.get(id=tag_id).name)
            except CategoryItem.DoesNotExist:
                pass
        return tags


class SiteConfiguration(SingletonModel):
    price_delivery = models.IntegerField(u'Цена доставки', default=0)
    card_days_expired = models.IntegerField(u'Срок действия карты, дней', default=31)

    def __unicode__(self):
        return u"Настройки сайта"

    class Meta:
        verbose_name = u"Настройки сайта"
        verbose_name_plural = u"Настройки сайта"


class Import(models.Model):
    class Meta:
        verbose_name = u"импорт"
        verbose_name_plural = u"импорты"

    csv_file = models.FileField(
        u'', upload_to='cvs/{}'.format(
            datetime.now().strftime('%Y-%m-%d-%H-%M-%S')))
    import_date = models.DateTimeField(u'', auto_now_add=True)

    def __unicode__(self):
        return self.csv_file.name


CATEGORY_ONLY = 0
CATEGORY_MULTI = 1

CATEGORY_TYPE = (
    (CATEGORY_ONLY, u'однозначный выбор'),
    (CATEGORY_MULTI, u'множественный выбор'),
)


class Category(models.Model):
    id = models.CharField(u'id', max_length=255, primary_key=True)
    name = models.CharField(u'Название', max_length=255)
    category_type = models.PositiveIntegerField(
        u'тип выбора', choices=CATEGORY_TYPE, default=CATEGORY_MULTI)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = u"категория"
        verbose_name_plural = u"категории"

    def __unicode__(self):
        return self.name


class CategoryItem(models.Model):
    id = models.CharField(u'id', max_length=255, primary_key=True)
    parent = models.ForeignKey(Category, related_name='items')
    name = models.CharField(u'Название', max_length=255)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "CategoryItem"
        verbose_name_plural = "CategoryItems"

    def __unicode__(self):
        return self.name


BASKET_OPEN = 0
BASKET_CLOSED = 1
BASKET_STATUS = (
    (BASKET_OPEN, u'открыта'),
    (BASKET_CLOSED, u'закрыта'),
)

DELIVERY = 0
EXTRA_SERVICE_TYPE = (
    (DELIVERY, u'доставка'),
)


class Basket(models.Model):
    session_id = models.CharField(u'Код сессии', max_length=255)
    created = models.DateTimeField(u'Создана', auto_now_add=True)
    modified = models.DateTimeField(u'Изменена', auto_now_add=True)
    status = models.IntegerField(
        u'статус', choices=BASKET_STATUS, default=BASKET_OPEN)

    class Meta:
        verbose_name = "корзина"
        verbose_name_plural = "корзины"
        ordering = ['modified', ]

    def __unicode__(self):
        return '{} - {}'.format(self.session_id, self.modified)

    @property
    def summa(self):
        summa = 0
        for item in self.items.all():
            summa += item.item_sum
        for item in self.extra_items.all():
            summa += item.price
        return summa

    @property
    def summa_to_pay(self):
        cards_summa = 0
        for transaction in self.transactions.all():
            cards_summa += transaction.summa
        return self.summa - cards_summa

    @property
    def item_count(self):
        count = 0
        for item in self.items.all():
            count += item.count
        return count

    def has_extra_service(self, service_type):
        return self.extra_items.filter(type=service_type).count()

    def add_card_transaction(self, card):
        if card.blocked or not card.nominal:
            return
        card.blocked = True
        card.save()

        if card.nominal >= self.summa_to_pay:
            transaction_summa = self.summa_to_pay
        else:
            transaction_summa = card.nominal

        CardTransaction.objects.create(
            basket=self,
            card=card,
            summa=transaction_summa
        )


class BasketItem(models.Model):
    basket = models.ForeignKey(Basket, related_name='items')
    item = models.ForeignKey(Entertainment)
    count = models.PositiveIntegerField(u'количество', default=1)

    class Meta:
        verbose_name = "элемент корзины"
        verbose_name_plural = "элементы корзины"

    def __unicode__(self):
        return self.item.name

    @property
    def item_sum(self):
        return self.item.price * self.count

    def save(self, *args, **kwargs):
        super(BasketItem, self).save(*args, **kwargs)
        if not self.count:
            self.delete()


class BasketExtaItem(models.Model):
    basket = models.ForeignKey(Basket, related_name='extra_items')
    type = models.IntegerField(u'тип', choices=EXTRA_SERVICE_TYPE, default=0)
    name = models.CharField(u'Название', max_length=255, blank=True)
    price = models.PositiveIntegerField(u'цена', default=0)

    class Meta:
        verbose_name = "доп.элемент корзины"
        verbose_name_plural = "доп.элементы корзины"


def fill_entertainments(sender, instance, created, *args, **kwargs):
    def get_unique_id(model_name):
        model = get_model('store', model_name)
        item_id = ''
        while not item_id:
            tmp_id = str(uuid.uuid4()).split('-')[0][:6]
            if not model.objects.filter(id=tmp_id).count():
                item_id = tmp_id
        return item_id

    def get_ent_tags(header, row):
        tags_ids = []
        for ind in range(11, len(row)):

            cat_name = unicode(header[ind].strip(), encoding='utf-8')
            if cat_name:
                if not Category.objects.filter(name=cat_name).count():
                    cat = Category.objects.create(name=cat_name, id=get_unique_id('Category'))
                else:
                    cat = Category.objects.get(name=cat_name)
                    cat.active = True
                cat.save()

                for item in row[ind].split(','):
                    cat_item_name = unicode(item.strip(), encoding='utf-8')
                    if cat_item_name:
                        if not CategoryItem.objects.filter(name=cat_item_name,
                                                           parent=cat):
                            cat_item = CategoryItem.objects.create(
                                name=cat_item_name, parent=cat,
                                id=get_unique_id('CategoryItem'))
                        else:
                            cat_item = CategoryItem.objects.get(
                                name=cat_item_name, parent=cat)
                            cat_item.active = True
                        cat_item.save()
                        tags_ids.append(cat_item.id)

        return ','.join(tags_ids)

    Entertainment.objects.all().update(active=False)
    Category.objects.all().update(active=False)
    CategoryItem.objects.all().update(active=False)

    with instance.csv_file as f:
        reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_ALL)
        header_row = reader.next()
        for items in reader:

            article, partner_name, name, explanation, ent_type, peoples, duration, conditions, \
                expiration_date, price, revenue = [unicode(i.strip(), 'UTF-8') for i in items[:11]]
            ent_type = ent_type.upper()

            if not ent_type == u'КАРТА':
                if '' in (article, partner_name, name, ent_type, expiration_date):
                    continue

            ent, created = Entertainment.objects.get_or_create(article=int(article))
            if not created:
                ent.active = True

            if ent_type != u'КАРТА':
                partner, created = Partner.objects.get_or_create(name=partner_name)
                ent.partner = partner

            ent.name = name
            ent.explanation = explanation

            if ent_type == u'УСЛУГА':
                ent.type = SERVICE
            elif ent_type == u'ТОВАР':
                ent.type = PRODUCT
            elif ent_type == u'КАРТА':
                ent.type = CARD

            ent.peoples = peoples
            ent.duration = duration
            ent.conditions = conditions
            ent.date_expiration = datetime.strptime(expiration_date, '%d.%m.%Y') if expiration_date else None
            ent.price = int(price) if price else 0
            ent.revenue = int(revenue) if revenue else 0
            ent.tags = get_ent_tags(header_row, items)
            ent.save()

            # except Exception, er:
            #     print er
            #     continue

    # Entertaiment.objects.filter(active=False).delete()
    # Category.objects.filter(active=False).delete()
    # CategoryItem.objects.filter(active=False).delete()


post_save.connect(fill_entertainments, sender=Import)

ORDER_NEW, ORDER_CHECKED, ORDER_PAYED = range(3)
ORDER_STATUS = (
    (ORDER_NEW, u'новый'),
    (ORDER_CHECKED, u'на оплату'),
    (ORDER_PAYED, u'оплачен'),
)


class Order(models.Model):
    username = models.CharField(u'имя', max_length=255)
    phone = models.CharField(u'телефон', max_length=12, blank=True)
    email = models.EmailField(u'email')
    status = models.IntegerField(u'статус', choices=ORDER_STATUS, default=ORDER_NEW)
    created = models.DateTimeField(u'создан', auto_now_add=True)

    class Meta:
        verbose_name = u'заказ'
        verbose_name_plural = u'заказы'

    def __unicode__(self):
        return u'Order {} {}'.format(self.id, self.created)

    @property
    def summa(self):
        summa = 0
        for item_list in (self.items.all(), self.extra_items.all()):
            for item in item_list:
                summa += item.price
        return summa

    @property
    def summa_to_pay(self):
        cards_summa = 0
        for transaction in self.transactions.all():
            cards_summa += transaction.summa
        return self.summa - cards_summa


ORDER_ITEM_NEW, ORDER_ITEM_ACTIVE, ORDER_ITEM_PAYED, ORDER_ITEM_WAIT_FOR_PAY, ORDER_ITEM_OVERDUE, ORDER_ITEM_REPAYED = range(6)
ORDER_ITEM_STATUS = (
    (ORDER_ITEM_NEW, u'новый'),
    (ORDER_ITEM_ACTIVE, u'активный'),
    (ORDER_ITEM_PAYED, u'оплачен'),
    (ORDER_ITEM_WAIT_FOR_PAY, u'ожидает оплаты'),
    (ORDER_ITEM_OVERDUE, u'просрочен'),
    (ORDER_ITEM_REPAYED, u'погашен')
)


CARD_ACTIVE, CARD_INACTIVE, CARD_EXPIRED = range(3)
CARD_STATUS = (
    (CARD_ACTIVE, u'активная'),
    (CARD_INACTIVE, u'неактивная'),
    (CARD_EXPIRED, u'просрочена'),
)


class Card(models.Model):
    login = models.CharField(u'логин', max_length=15)
    password = models.CharField(u'пароль', max_length=15)
    nominal = models.PositiveIntegerField(u'сумма', default=0)
    date_expired = models.DateField(u'дата годности')
    status = models.PositiveIntegerField(u'статус', choices=CARD_STATUS, default=CARD_ACTIVE)
    blocked = models.BooleanField(u'заблокирована', default=False)

    class Meta:
        verbose_name = u'подарочная карта'
        verbose_name_plural = u'подарочные карты'

    def __unicode__(self):
        return '{}({})'.format(self.id, self.date_expired.strftime('%d.%m.%Y'))

    @property
    def balance(self):
        card_sum_operations = self.transactions.aggregate(total_sum=Sum('summa'))['total_sum']
        return self.nominal - (card_sum_operations if card_sum_operations else 0)

    @classmethod
    def create_card(cls, nominal):
        return cls.objects.create(
            nominal=nominal,
            login=_generate_code(6),
            password=_generate_code(8),
            date_expired=date.today()+timedelta(days=SiteConfiguration.get_solo().card_days_expired)
        )


class OrderItem(EntertainmentBase):
    order = models.ForeignKey(Order, related_name='items')
    partner = models.ForeignKey(Partner, related_name='ordered_items', null=True, verbose_name=u'Партнер')
    date_redemption = models.DateField(u'Дата погашения', blank=True, null=True)
    status = models.IntegerField(u'Статус', choices=ORDER_ITEM_STATUS, default=ORDER_ITEM_NEW)
    code = models.CharField(u'Код', max_length=15, blank=True)
    card = models.ForeignKey(Card, blank=True, null=True)

    class Meta:
        verbose_name = u'единица заказа'
        verbose_name_plural = u'единицы заказа'

    def generate_number(self):
        while True:
            code = str(uuid.uuid4()).split('-')[0]
            if not OrderItem.objects.filter(code=code):
                self.code = code
                self.save()
                return

    def __unicode__(self):
        return u'{} - {}'.format(self.name, self.order)


class OrderExtraItem(models.Model):
    order = models.ForeignKey(Order, related_name='extra_items')
    type = models.IntegerField(u'тип', choices=EXTRA_SERVICE_TYPE, default=0)
    name = models.CharField(u'Название', max_length=255, blank=True)
    price = models.PositiveIntegerField(u'цена', default=0)


TRANSACTION_NEW, TRANSACTION_ACCEPTED = range(2)
TRANSACTION_STATUS = (
    (TRANSACTION_NEW, u'новая'),
    (TRANSACTION_ACCEPTED, u'проведена'),
)


class CardTransaction(models.Model):
    order = models.ForeignKey(Order,related_name='transactions', null=True)
    basket = models.ForeignKey(Basket,related_name='transactions', null=True)
    card = models.ForeignKey(Card, related_name='transactions')
    summa = models.PositiveIntegerField(u'сумма')
    status = models.IntegerField(u'', choices=TRANSACTION_STATUS, default=TRANSACTION_NEW)
    date_operation = models.DateTimeField(u'дата', auto_now_add=True)
    info = models.TextField(u'информация', blank=True)

    class Meta:
        verbose_name = u'операция'
        verbose_name_plural = u'операции'